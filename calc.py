# Funcion sumar
def sumar(x1: float, x2: float) -> float:
    """ Sumar dos números """
    return x1 + x2


# Funcion restar
def restar(x1: float, x2: float) -> float:
    """ Restar dos números """
    return abs(x1 - x2)


# Probando las funciones
print("Sumar 1+2: " + str(sumar(1, 2)))
print("Sumar 3+4: " + str(sumar(3, 4)))
print("Restar 6-5: " + str(restar(5, 6)))
print("Sumar 8-7: " + str(restar(8, 7)))
